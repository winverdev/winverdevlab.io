# ぺぽよ_PEPOYO作品镜像

本仓库存放ぺぽよ_PEPOYO作品，供无法打开Pixiv的地区使用

Media:音乐及其MV

[Illustration](https://github.com/hjm2007/ppymirror/tree/main/Illustration):插画

[Manga](https://github.com/hjm2007/ppymirror/tree/main/Manga):漫画

作品名称请打开"PhotoList"文件夹，提供csv和json两种格式作品列表，新手请查看csv文件

如下为曲目列表直链:

[2020曲目](https://hjm2007.github.io/ppymirror/Media/2020list)

[2021曲目](https://hjm2007.github.io/ppymirror/Media/2021list)

[2022曲目](https://hjm2007.github.io/ppymirror/Media/2022list)

[2023曲目](https://hjm2007.github.io/ppymirror/Media/2023list)

针对中国特供版(即带有中文水印)请访问如下地址:

https://space.bilibili.com/3461574945671798/dynamic

https://pepoyoppy.lofter.com/


使用前请阅读[二创守则](https://github.com/hjm2007/ppymirror/wiki)

注意：为贯彻二创守则，本仓库不包含FANBOX付费内容！！！！

## 因为文件过多，所以一时新更改无法加入，请等一段时间再刷新查看！！！

如果中国大陆无法访问github.com或github.io域名，请访问如下镜像站(基于Cloudflare Worker):

[Media](https://ppymirror.hhaann.eu.org/mirror)

[Illustration](https://github.hhaann.eu.org/hjm2007/ppymirror/tree/main/Illustration)

[Manga](https://github.hhaann.eu.org/hjm2007/ppymirror/tree/main/Manga)

相关文件均来自:

图片：https://www.pixiv.net/users/6592374

视频：https://www.youtube.com/playlist?list=PLirTIukOmzhBVh0aygh46EZlZ6jtlLlDm

音乐均使用工具下载

最后，送给小鬼一句与政治无关的话：

# 不要野蛮要文明，不要文革要改革，不要造谣要事实，不要领袖要选票，不要谎言要尊严，不做小鬼做良民
