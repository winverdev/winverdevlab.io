## Music 

- [ぺぽよ-なめくじ事録.mp3](https://hjm2007.github.io/ppymirror/Media/2023/music/%E3%81%BA%E3%81%BD%E3%82%88-%E3%81%AA%E3%82%81%E3%81%8F%E3%81%98%E4%BA%8B%E9%8C%B2.mp3)
- [ぺぽよ-アスペル⁂ガーデン.mp3](https://hjm2007.github.io/ppymirror/Media/2023/music/%E3%81%BA%E3%81%BD%E3%82%88-%E3%82%A2%E3%82%B9%E3%83%9A%E3%83%AB%E2%81%82%E3%82%AC%E3%83%BC%E3%83%87%E3%83%B3.mp3)
- [ぺぽよ-拝啓.mp3](https://hjm2007.github.io/ppymirror/Media/2023/music/%E3%81%BA%E3%81%BD%E3%82%88-%E6%8B%9D%E5%95%93.mp3)

## Video

- [2023年の曲をまとめたよ＋申し訳程度の解説.mp4](https://hjm2007.github.io/ppymirror/Media/2023/video/2023年の曲をまとめたよ＋申し訳程度の解説.mp4)
- [【ねこのティーチくん】終演の猫 feat.ティーチくん【テーマソング】.mp4](https://hjm2007.github.io/ppymirror/Media/2023/video/終演の猫.mp4)
- [わたしだけの桃幻郷 feat.VY1_蒼姫ラピス.mp4](https://hjm2007.github.io/ppymirror/Media/2023/video/わたしだけの桃幻郷.mp4)
- [なめくじ事録 ♪ぽよろいど.mp4](https://hjm2007.github.io/ppymirror/Media/2023/video/なめくじ事録.mp4)
- [アスペル⁂ガーデン ♪VY1.mp4](https://hjm2007.github.io/ppymirror/Media/2023/video/アスペル⁂ガーデン.mp4)
- [拝啓 ♪初音ミク.mp4](https://hjm2007.github.io/ppymirror/Media/2023/video/拝啓.mp4)
